package com.example.zafar.rmdb;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class FetchData extends AsyncTask <String, Void, Void> {
    private static final String TAG = "fetchData";
    String singlParsed, data;
    //    ArrayAdapter<String> arrayAdapter;
    int n = 0;
    private ArrayList <String> mTitles      = new ArrayList <> ();
    private ArrayList <String> mImageUrls   = new ArrayList <> ();
    private ArrayList <String> mYears       = new ArrayList <> ();
    private ArrayList <String> mType        = new ArrayList <> ();
    private Activity c;
    private ListView listView;

    @Override
    protected Void doInBackground(String... strings) {
        try {
            Log.d ( TAG , "doInBackground: Start" );
            URL url = new URL ( "http://www.omdbapi.com/?s=" + strings[0] + "&apikey=6578f1de" );

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection ();
            InputStream inputStream = httpURLConnection.getInputStream ();
            BufferedReader bufferedReader = new BufferedReader ( new InputStreamReader ( inputStream ) );
            JSONArray ja = new JSONArray ( "[" + (data = bufferedReader.readLine ()) + ",{}]" );
            JSONObject jsonObject = (JSONObject) ja.get ( 0 );
            Log.d ( TAG , "doInBackground: before IF" + data );
            if (jsonObject.get ( "Response" ).equals ( "False" )) {
                mTitles.add ( strings[0] + " : Not Available " );
                mYears.add ( "" );
                mType.add ( "" );
                mImageUrls.add ( "https://ibb.co/Fw48Hff" );
            } else {
                JSONArray jAobj = new JSONArray ( (jsonObject.get ( "Search" ).toString ()) );
                n = jAobj.length ();
                int i = 0;
                for (i = 0; i < n; i++) {
                    JSONObject jsonObject1 = (JSONObject) jAobj.get ( i );

                    //    Log.d ( "IQ" , "DDD8" + jsonObject1 + "  "  + "\n" );
                    mTitles.add ( (String) jsonObject1.get ( "Title" ) );
                    Log.d ( TAG , "doInBackground: " + i + "\n\n" );
                    mImageUrls.add ( (String) jsonObject1.get ( "Poster" ) );
                    mYears.add ( (String) jsonObject1.get ( "Year" ) );
                    mType.add ( (String) jsonObject1.get ( "Type" ) );
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace ();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute ( aVoid );

        MoviesListViewAdapter adapter = new MoviesListViewAdapter ( c , mTitles , mImageUrls , mYears , mType );
        listView.setAdapter ( adapter );
    }

    public void initListView(ListView recyclerView , Activity context) {

        c = context;
        listView = recyclerView;
    }
}
