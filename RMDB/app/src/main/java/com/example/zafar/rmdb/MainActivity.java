package com.example.zafar.rmdb;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    String string = "Hellboy";
    ImageButton imageView;
    ListView listView;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );

        listView    = findViewById ( R.id.moviesListView );
        editText    = findViewById ( R.id.editText );
        imageView   = findViewById ( R.id.imageButton1 );

        editText.setFilters ( new InputFilter[]{new InputFilter.LengthFilter ( 25 )} );

        fetchingData ();

        imageView.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                string = editText.getText ().toString ();
                string = string.trim ().replaceAll ( "\\s+" , " " );
                fetchingData ();
            }
        } );
        editText.setOnEditorActionListener ( new TextView.OnEditorActionListener () {
            @Override
            public boolean onEditorAction(TextView v , int actionId , KeyEvent event) {
                fetchingData ();
                return true;
            }
        } );
    }

    public void fetchingData() {
        FetchData process = new FetchData ();
        process.initListView ( listView , MainActivity.this );
        process.execute ( string );
    }
}
