package com.example.zafar.rmdb;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MovieDetailsListViewAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList <String> values;
    private String[] valueTypes = {"Poster", "Title", "Date and Genre", "Runtime", "Director",
            "Writer", "Actors", "Languages", "Country" };

    public MovieDetailsListViewAdapter(Activity context , ArrayList <String> values ) {
        this.context    = context;
        this.values     = values;
    }

    @Override
    public int getCount() {
        return valueTypes.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent) {
        TextView typeTextView;
        TextView valueTextView;
        ImageView posterImageView;
        switch(position){
            case 0 :
                convertView = LayoutInflater.from ( context ).inflate ( R.layout.image_item , null );
                posterImageView = convertView.findViewById ( R.id.imageC );
                Glide.with ( context )
                        .asBitmap ()
                        .load ( values.get ( position ) )
                        .into ( posterImageView );
                break;
            case 1 :
                convertView = LayoutInflater.from ( context ).inflate ( R.layout.title_item , null );
                valueTextView = convertView.findViewById ( R.id.titleC );
                valueTextView.setText ( values.get ( position ) );
                break;
            case 2:
                convertView = LayoutInflater.from ( context ).inflate ( R.layout.year_genre_item , null );
                valueTextView = convertView.findViewById ( R.id.rDateC );
                valueTextView.setText ( values.get ( position ) + "  ,  " + values.get ( position + 1 ) );
                break;
            default :
                convertView = LayoutInflater.from ( context ).inflate ( R.layout.movie_attribute_item , null );
                typeTextView    = convertView.findViewById ( R.id.typeTextViewID );
                valueTextView   = convertView.findViewById ( R.id.typeValueTextViewID );

                typeTextView.setText ( valueTypes[position] );
                valueTextView.setText ( values.get ( position + 1 )  );
                break;
        }


        return convertView;
    }
}