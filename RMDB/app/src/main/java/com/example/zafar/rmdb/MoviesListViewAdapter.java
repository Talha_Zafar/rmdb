package com.example.zafar.rmdb;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MoviesListViewAdapter extends ArrayAdapter {

    private ArrayList <String> mTitle;
    private ArrayList <String> mImages;
    private ArrayList <String> mYear;
    private ArrayList <String> mType;
    private Activity mContext;

    public MoviesListViewAdapter(Activity context ,
                                 ArrayList <String> mTitles , ArrayList <String> mImageUrls ,
                                 ArrayList <String> mYears , ArrayList <String> mType) {
        super ( context , R.layout.layout_list_item , mImageUrls );

        this.mTitle     = mTitles;
        this.mYear      = mYears;
        this.mType      = mType;
        this.mImages    = mImageUrls;
        this.mContext   = context;
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from ( mContext ).inflate ( R.layout.layout_list_item , null );
            holder = new ViewHolder ( convertView );
            convertView.setTag ( holder );
        } else {
            holder = (ViewHolder) convertView.getTag ();
        }

        onBindViewHolder ( holder , position );

        return convertView;
    }

    private void onBindViewHolder(@NonNull MoviesListViewAdapter.ViewHolder viewHolder , final int i) {
        Glide.with ( mContext )
                .asBitmap ()
                .load ( mImages.get ( i ) )
                .into ( viewHolder.image );

        viewHolder.mTiTle.setText ( mTitle.get ( i ) );
        viewHolder.typeAndYearTextView.setText ( mType.get ( i ) + " (" + mYear.get ( i ) + ")" );
        viewHolder.parentLayout.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent ( mContext , MovieActivity.class );
                intent.putExtra ( "mTitle" , mTitle.get ( i ) );
                intent.putExtra ( "year" , mYear.get ( i ) );
                mContext.startActivity ( intent );

                Toast.makeText ( mContext , mTitle.get ( i ) , Toast.LENGTH_SHORT ).show ();
            }
        } );
    }

    public static class ViewHolder {
        ImageView image;
        TextView mTiTle;
        TextView typeAndYearTextView;
        RelativeLayout parentLayout;

        ViewHolder(View itemView) {
            image               = itemView.findViewById ( R.id.posterImageViewID );
            mTiTle              = itemView.findViewById ( R.id.movieTitleTextViewID );
            typeAndYearTextView = itemView.findViewById ( R.id.typeAndYearTextViewID );
            parentLayout        = itemView.findViewById ( R.id.parent_layout );
        }
    }

}