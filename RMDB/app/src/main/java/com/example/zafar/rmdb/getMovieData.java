package com.example.zafar.rmdb;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetMovieData extends AsyncTask <String, Void, Void> {

    private ArrayList <String> values = new ArrayList <> ();
    private Context context;
    private ListView listView;

    @Override
    protected Void doInBackground(String... strings) {
        try {
            URL url = new URL ( "http://www.omdbapi.com/?t=" + strings[0] +
                    "&y=" + strings[1] + "&apikey=6578f1de" );

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection ();

            InputStream inputStream = httpURLConnection.getInputStream ();

            BufferedReader bufferedReader = new BufferedReader ( new InputStreamReader ( inputStream ) );
            JSONArray ja = new JSONArray ( "[" + (bufferedReader.readLine ()) + ",{}]" );
            JSONObject jsonObject = (JSONObject) ja.get ( 0 );
            if (jsonObject.get ( "Response" ).equals ( "False" )) {

                values.add ( "https://ibb.co/Fw48Hff" );
                values.add ( strings[0] + " : Not Available " );
                values.add ( "" );
                values.add ( "" );
                values.add ( "" );
                values.add ( "" );
                values.add ( "" );
                values.add ( "" );
                values.add ( "" );
                values.add ( "" );

            } else {
                values.add ( (String) jsonObject.get ( "Poster" ) );
                values.add ( (String) jsonObject.get ( "Title" ) );
                values.add ( (String) jsonObject.get ( "Released" ) );
                values.add ( (String) jsonObject.get ( "Genre" ) );
                values.add ( (String) jsonObject.get ( "Runtime" ) );
                values.add ( (String) jsonObject.get ( "Director" ) );
                values.add ( (String) jsonObject.get ( "Writer" ) );
                values.add ( (String) jsonObject.get ( "Actors" ) );
                values.add ( (String) jsonObject.get ( "Language" ) );
                values.add ( (String) jsonObject.get ( "Country" ) );
            }
        } catch (IOException | JSONException e1) {
            e1.printStackTrace ();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute ( aVoid );

        MovieDetailsListViewAdapter adapter = new MovieDetailsListViewAdapter ( (Activity) context , values );
        listView.setAdapter ( adapter );
    }

    public void viewInitializer(Activity context , ListView listView) {
        this.listView = listView;
        this.context = context;
    }
}
