package com.example.zafar.rmdb;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MovieActivity extends AppCompatActivity {

    ListView listView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.movie_activity );

        listView = findViewById ( R.id.attributesListView );

        GetMovieData process = new GetMovieData ();

        process.viewInitializer(this, listView);
        process.execute ( getIntent ().getStringExtra ( "mTitle" ),
                getIntent ().getStringExtra ( "year" ) );

    }
}
